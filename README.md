# generator-vx [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Vue + Rollup generator

## Installation

First, install [Yeoman](http://yeoman.io) and generator-vx using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-vx
```

Then generate your new project:

```bash
yo vx
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [to1source](to1source.cn)


[npm-image]: https://badge.fury.io/js/generator-vx.svg
[npm-url]: https://npmjs.org/package/generator-vx
[travis-image]: https://travis-ci.org/yiyuan-to1source/generator-vx.svg?branch=master
[travis-url]: https://travis-ci.org/yiyuan-to1source/generator-vx
[daviddm-image]: https://david-dm.org/yiyuan-to1source/generator-vx.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/yiyuan-to1source/generator-vx
